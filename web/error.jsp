<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error JSP</title>
        <link rel="stylesheet" href="styles.css" type="text/css">
    </head>
    <body>
        <%
            Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
            out.print("Ошибка " + throwable.toString());
        %>
    </body>
</html>