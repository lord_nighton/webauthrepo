<%@page import="java.util.ArrayList"%>
<%@page import="com.lordnighton.users.User"%>
<%@page import="com.lordnighton.users.SystemUsers"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.lordnighton.constants.WeAppConstants.*"%>
<!DOCTYPE html>
<%!
    public void printErrorDiv (String message, JspWriter out) throws Exception {
        out.println("<center><div id='errorDiv'><h2>" + message + "</h2></div></center>");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add messages page</title>
        <link rel="stylesheet" href="styles.css" type="text/css">
    </head>
    <body>
<%
    String login = (String) session.getAttribute("login"); 
    if (null == login || true == login.isEmpty()) {
        response.sendRedirect("login.jsp");
    } else { 
        SystemUsers users = (SystemUsers) getServletContext().getAttribute("users");
        if (null == users || 0 == users.getUsers().size()) {
            printErrorDiv (UNDEFINED_USERS_LIST, out);
%>
                <script>
                    setTimeout(function() {
                        window.location.href="login.jsp";
                    }, 2000);
                </script>
<%
        } else {
            User user = users.getUserByLogin (login);
            if (null == user) {
                printErrorDiv (UNDEFINED_USER_MESSAGE, out);
%>
                    <script>
                        setTimeout(function() {
                            window.location.href="login.jsp";
                        }, 2000);
                    </script>
<%
            } else { 
%>
        <center>
            <h2>Добавьте необходимые сообщения</h2>
            Логин пользователя = <%= login%> <br /><br />
            <form action="MessagesServlet" method="get">
                <input name="<%= MESSAGE_PARAM%>" type="text" value="" />
                <input name = "add" type="submit" value="Добавить" />
                <input name = "exit" type="submit" value="Выйти" />
            </form>
        </center>
        
        Сообщения пользователя = 
        <ul>
        </ul>
<% 
                ArrayList <String> messages = user.getMessages();
                for (String next : messages) {
%>
                <li> 
                    <%= next%>
                </li>
<% 
                }
            }
        }
    }
%>
    </body>
</html>