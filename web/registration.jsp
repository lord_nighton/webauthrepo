<%@page import="com.lordnighton.users.UserImpl"%>
<%@page import="com.lordnighton.users.User"%>
<%@page import="com.lordnighton.users.SystemUsers"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.lordnighton.constants.WeAppConstants.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
        <link rel="stylesheet" href="styles.css" type="text/css">
    </head>
    <body>
        <center>
<%
            SystemUsers sysUsers = (SystemUsers) getServletContext().getAttribute(USERS_PARAM);
%>
            <h3>Пользователи системы</h3>
            <div>
                <%= sysUsers%>
            </div>
            
            <hr>
            
            <h2>Регистрация нового пользователя</h2>
            <div>
                <form name="<%= REGISTER_PARAM%>" action="RegistrationServlet" method="get">
                    <input name="<%= NEW_USER_LOGIN_PARAM%>" type="text" value="" />
                    <input type="submit" value="Зарегистрировать" />
                </form>
            </div>
        </center>
    </body>
</html>