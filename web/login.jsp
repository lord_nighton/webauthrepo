<%@page import="com.lordnighton.users.UserImpl"%>
<%@page import="com.lordnighton.users.User"%>
<%@page import="com.lordnighton.users.SystemUsers"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.lordnighton.constants.WeAppConstants.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <link rel="stylesheet" href="styles.css" type="text/css">
    </head>
    <body>
        <center>
<%
            SystemUsers sysUsers = (SystemUsers) getServletContext().getAttribute(USERS_PARAM);
%>
            <h3>Пользователи системы</h3>
            <div>
                <%= sysUsers%>
            </div>
            <hr>
            
            <h2>Вход в систему</h2>
            <div>
                <form name="<%= LOGIN_PARAM%>" action="LoginServlet" method="get">
                    <input name="<%= LOGINNED_USER_LOGIN_PARAM%>" type="text" value="" />
                    <input name="<%= LOGIN_PARAM%>" type="submit" value="Вход" />
                </form>
            </div>
        </center>
    </body>
</html>