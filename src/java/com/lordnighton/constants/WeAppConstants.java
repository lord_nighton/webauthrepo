package com.lordnighton.constants;

/**
 *
 * @author LordNighton
 * 
 */

public interface WeAppConstants {
 
    public static final String USERS_PARAM = "users";

    public static final String LOGOUT_PARAM = "logout";
    
    public static final String LOGIN_PARAM = "login";

    public static final String REGISTER_PARAM = "register";
    
    public static final String MESSAGE_PARAM = "message";

    public static final String ADD_MESSAGE_PARAM = "addMessage";
    
    public static final String NEW_USER_LOGIN_PARAM = "newUserLogin";
    
    public static final String LOGINNED_USER_LOGIN_PARAM = "loginnedUserLogin";

    public static final String UNDEFINED_USER_MESSAGE = "Невозможно определить логин пользователя!";

    public static final String UNDEFINED_USERS_LIST = "Невозможно определить список пользователей!";
    
}