package com.lordnighton.users;

import java.util.ArrayList;

/**
 * 
 * @author LordNighton
 * 
 */

public class UserImpl implements User {

    private String login;
    
    private ArrayList <String> messages = new ArrayList <String> ();

    @Override
    public String getLogin() {
        return this.login;
    }
    
    @Override
    public void setLogin (String login) {
        this.login = login;
    }

    @Override
    public ArrayList <String> getMessages() {
        return this.messages;
    }
    
    @Override
    public void addMessage (String message) {
        this.getMessages().add(message);
    }
    
}