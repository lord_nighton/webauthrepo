package com.lordnighton.users;

import java.util.ArrayList;

/*
 *
 * @author LordNighton
 * 
 */

public interface User {

    public String getLogin ();
    
    public void setLogin (String login);
    
    public ArrayList <String> getMessages ();
    
    public void addMessage (String message);
    
}