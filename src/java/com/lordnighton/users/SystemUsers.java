package com.lordnighton.users;

import java.util.ArrayList;

/**
 *
 * @author LordNighton
 *
 */

public class SystemUsers {

    private ArrayList <User> users = new ArrayList <User> ();
    
    public ArrayList <User> getUsers () {
        return this.users;
    }
    
    public void addUser (User newUser) {
        this.users.add(newUser);
    }
    
    public User getUserByLogin (String login) {
        for (User next : users) {
            if (true == login.equals (next.getLogin())) {
                return next;
            }
        }
        
        return null;
    } 
    
    @Override
    public String toString () {
        String result = "";
        for (User next : users) {
            result += "Пользователь [" + next.getLogin() + "]<br />";
        }
        return result;
    }
            
}