package com.lordnighton.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.lordnighton.users.SystemUsers;
import com.lordnighton.users.User;
import javax.servlet.http.HttpSession;

import static com.lordnighton.constants.WeAppConstants.*;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(name = "MessagesServlet", urlPatterns = {"/MessagesServlet"})
public class MessagesServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        if (true == isAddMessageButtonPressed (request)) {
            SystemUsers users = (SystemUsers) request.getServletContext().getAttribute("users");
        
            String message = (String) request.getParameter(MESSAGE_PARAM);
        
            HttpSession session = request.getSession();
            String login = (String) session.getAttribute("login");

            User user = users.getUserByLogin(login);
            if (null != user) {
                user.addMessage(message);
            }
        }
        
        if (true == isExitButtonPressed (request)) {
            HttpSession session = request.getSession();
            session.removeAttribute("login");
        }
        
        response.sendRedirect("messages.jsp");
    }
    
    private boolean isAddMessageButtonPressed (HttpServletRequest request) {
        String add = (String) request.getParameter("add");
        String exit = (String) request.getParameter("exit");
        
        if (null != add && null == exit) {
            return true;
        } else {
            return false;
        }
    } 
    
    private boolean isExitButtonPressed (HttpServletRequest request) {
        String add = (String) request.getParameter("add");
        String exit = (String) request.getParameter("exit");
        
        if (null == add && null != exit) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}