package com.lordnighton.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.lordnighton.users.SystemUsers;
import com.lordnighton.users.User;
import com.lordnighton.users.UserImpl;

import static com.lordnighton.constants.WeAppConstants.*;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(urlPatterns = {"/RegistrationServlet"})
public class RegistrationServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String newUserLogin = (String) request.getParameter(NEW_USER_LOGIN_PARAM);
        
        SystemUsers users = (SystemUsers) request.getServletContext().getAttribute("users");
        
        User user = users.getUserByLogin(newUserLogin);
        if (null == user) {
            User newUser = new UserImpl();
            newUser.setLogin (newUserLogin);
            users.addUser (newUser);
        }
        
        response.sendRedirect("login.jsp");
    }

    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo() {
        return "Registration servlet";
    }
    
}