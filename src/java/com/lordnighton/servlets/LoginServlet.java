package com.lordnighton.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.lordnighton.users.SystemUsers;

import static com.lordnighton.constants.WeAppConstants.*;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        String login = (String) request.getParameter(LOGINNED_USER_LOGIN_PARAM);
        SystemUsers users = (SystemUsers) request.getServletContext().getAttribute("users");
        
        if (null == users.getUserByLogin(login)) {
            response.sendRedirect ("registration.jsp");
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("login", login);
            
            response.sendRedirect ("messages.jsp");
        }
    }        

    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo () {
        return "Login Servlet";
    }
    
}