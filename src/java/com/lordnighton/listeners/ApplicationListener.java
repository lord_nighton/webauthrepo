package com.lordnighton.listeners;

import com.lordnighton.users.SystemUsers;
import com.lordnighton.users.User;
import com.lordnighton.users.UserImpl;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Web application lifecycle listener.
 *
 * @author LordNighton
 * 
 */

@WebListener()
public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextInitialized (ServletContextEvent sce) {
        SystemUsers users = (SystemUsers) sce.getServletContext().getAttribute("users");
        
        User defaultUser = new UserImpl ();
        defaultUser.setLogin ("test");
        
        if (null == users) {
            users = new SystemUsers();
        }
        
        users.addUser(defaultUser);
        sce.getServletContext().setAttribute("users", users);
    }

    @Override
    public void contextDestroyed (ServletContextEvent sce) {
    }
    
}